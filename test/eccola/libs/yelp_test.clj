(ns eccola.libs.yelp-test
  (:require [clojure.test :refer :all]
            [eccola.libs.yelp :as yelp]))

(defn in?
  "Returns true if coll contains element."
  [coll element]
  (some #(= element %) coll))

(deftest search-parameters
  (testing "radius_filter reflects meters"
    (let [params {:distance 1}]
      (is (<= 1609 (get (yelp/search-parameters params) "radius_filter")))))

  (testing "radius_filter never exceeds 40225 meters"
    (let [params {:distance 99999}]
      (is (<= 40225 (get (yelp/search-parameters params) "radius_filter")))))

  (testing "category_filter reflects a random cuisine when using 'surprise_me'"
    (let [params {:cuisine "surprise_me"}]
     (is (in? (map first yelp/cuisines)
              (get (yelp/search-parameters params) "category_filter"))))))
