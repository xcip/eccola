(ns eccola.views.home-test
  (:require [clojure.test :refer :all]
            [eccola.views.home :as view]))

(def business
  {:snippet_image_url
   "http://s3-media1.fl.yelpcdn.com/photo/7L8zDud5ljjENa0xkYZyMA/ms.jpg",
   :is_claimed true,
   :phone "2129898737",
   :is_closed false,
   :name "Tres Carnes",
   :image_url
   "http://s3-media3.fl.yelpcdn.com/bphoto/p4wV13qpRTxXgmRxpABXJw/ms.jpg",
   :rating_img_url
   "http://s3-media4.fl.yelpcdn.com/assets/2/www/img/c2f3dd9799a5/ico/stars/v1/stars_4.png",
   :categories [["Tex-Mex" "tex-mex"] ["Mexican" "mexican"]],
   :id "tres-carnes-new-york",
   :url "http://www.yelp.com/biz/tres-carnes-new-york",
   :mobile_url "http://m.yelp.com/biz/tres-carnes-new-york",
   :snippet_text
   "This was my first time to Tres Carnes and I will for sure be returning.",
   :rating_img_url_small
   "http://s3-media4.fl.yelpcdn.com/assets/2/www/img/f62a5be2f902/ico/stars/v1/stars_small_4.png",
   :distance 3426.147644887494,
   :display_phone "+1-212-989-8737",
   :location
   {:postal_code "10010",
    :address ["688 6th Ave"],
    :geo_accuracy 9,
    :city "New York",
    :display_address ["688 6th Ave" "Flatiron" "New York, NY 10010"],
    :country_code "US",
    :coordinate {:latitude 40.7420235, :longitude -73.9934151},
    :neighborhoods ["Flatiron"],
    :state_code "NY"},
   :review_count 169,
   :rating_img_url_large
   "http://s3-media2.fl.yelpcdn.com/assets/2/www/img/ccf2b76faa2c/ico/stars/v1/stars_large_4.png",
   :rating 4.0})

(def search-results {:businesses [business]})

(deftest daddr
  (testing "returns the correct destination address"
    (is (= "688 6th Ave, New York, NY 10010"
           (view/daddr (:location business))))))

(deftest directions-uri
  (testing "returns the correct url-encoded uri"
    (let [daddr "688%206th%20Ave%2C%20New%20York%2C%20NY%2010010"]
      (is (= (str "http://maps.google.com/maps?saddr=Current%20Location&daddr=" daddr)
           (view/directions-uri business))))))

(deftest format-restaurants
  (testing "returned map contains correct values"
    (is (= {:name "Tres Carnes",
             :rating_img_url
              "http://s3-media4.fl.yelpcdn.com/assets/2/www/img/c2f3dd9799a5/ico/stars/v1/stars_4.png",
              :rating 4.0,
              :review_count 169,
              :mobile_url "http://m.yelp.com/biz/tres-carnes-new-york",
              :image_url
              "http://s3-media3.fl.yelpcdn.com/bphoto/p4wV13qpRTxXgmRxpABXJw/ms.jpg",
              :directions_uri
              "http://maps.google.com/maps?saddr=Current%20Location&daddr=688%206th%20Ave%2C%20New%20York%2C%20NY%2010010"}
            (view/format-restaurants business))))

  (testing "default yelp image is used when no image is provided"
    (is (= "http://s3-media2.ak.yelpcdn.com/bphoto/7DIHu8a0AHhw-BffrDIxPA/ms.jpg"
           (:image_url (view/format-restaurants (dissoc business :image_url)))))))

(deftest search-results->restaurants
  (testing "formats the search results into restaurants"
    (is (= "Tres Carnes"
           (:name (first (view/search-results->restaurants search-results)))))))

(deftest index
  (testing "when no params are supplied"
    (let [params {}
          html (view/index params {})]
      (testing "restaurants do not show up"
        (is (not (.contains html "Restaurant Name"))))
      (testing "data credits do not show up"
        (is (not (.contains html "data-credits"))))))

  (testing "when bogus params are supplied"
    (let [params {:cuisine "nonsense"}
          html (view/index params {})]
      (testing "a notice indicating 'no results' is displayed"
        (is (.contains html "No results...")))
      (testing "data credits do not show up"
        (is (not (.contains html "data-credits"))))))

  (testing "when authentic params are supplied"
    (let [params {:cuisine "newamerican"
                  :distance "5"
                  :latitude 40.7281
                  :longitude -84.0776}
          html (view/index params {})]
      (testing "restaurant results are displayed"
        (is (.contains html "restaurant-name")))
      (testing "data credits show up"
        (is (.contains html "data-credits"))))))
