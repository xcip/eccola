# eccola

A restaurant suggestion service for those times when you're too hungry to sift through hundreds of reviews.

## Usage

You'll need a `.lein-env` file in the project root:

```
{:hot-reload? true
 :yelp-consumer-key    "CHANGE_ME"
 :yelp-consumer-secret "CHANGE_ME"
 :yelp-token           "CHANGE_ME"
 :yelp-token-secret    "CHANGE_ME"}
```

`:hot-reload? true` enables hot-reloading, which makes developing much more pleasant. To obtain Yelp API keys, please visit their [developer website](http://www.yelp.com/developers).

Run it with `lein run`.

## Tests

Run them with `lein test`.

## Example nginx Configuration

```
    upstream eccola_server {
        server 127.0.0.1:9090;
        keepalive 8;
    }

    server {
        server_name eccola.echodrop.net;
        root /srv/http/eccola;

        location / {
            proxy_pass  http://eccola_server;

            proxy_http_version 1.1;
            proxy_set_header Connection "";

            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;

            access_log  /srv/http/logs/eccola/access.log;
            error_log   /srv/http/logs/eccola/error.log;
        }
    }
```
