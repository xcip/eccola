(ns eccola.libs.yelp
  (:require [gws.yelp.client :as client]
            [eccola.libs.calculations :refer [miles->meters]]))

(def cuisines
  [["afghani"          "Afghan"]
   ["african"          "African"]
   ["newamerican"      "American (New)"]
   ["tradamerican"     "American (Traditional)"]
   ["argentine"        "Argentine"]
   ["asianfusion"      "Asian Fusion"]
   ["bbq"              "Barbeque"]
   ["basque"           "Basque"]
   ["belgian"          "Belgian"]
   ["brasseries"       "Brasseries"]
   ["brazilian"        "Brazilian"]
   ["breakfast_brunch" "Breakfast & Brunch"]
   ["british"          "British"]
   ["buffets"          "Buffets"]
   ["burgers"          "Burgers"]
   ["burmese"          "Burmese"]
   ["cafes"            "Cafes"]
   ["cajun"            "Cajun/Creole"]
   ["cambodian"        "Cambodian"]
   ["caribbean"        "Caribbean"]
   ["cheesesteaks"     "Cheesesteaks"]
   ["chicken_wings"    "Chicken Wings"]
   ["chinese"          "Chinese"]
   ["creperies"        "Creperies"]
   ["cuban"            "Cuban"]
   ["delis"            "Delis"]
   ["dimsum"           "Dim Sum"]
   ["diners"           "Diners"]
   ["ethiopian"        "Ethiopian"]
   ["hotdogs"          "Fast Food"]
   ["filipino"         "Filipino"]
   ["fishnchips"       "Fish & Chips"]
   ["fondue"           "Fondue"]
   ["foodstands"       "Food Stands"]
   ["french"           "French"]
   ["gastropubs"       "Gastropubs"]
   ["german"           "German"]
   ["gluten_free"      "Gluten-Free"]
   ["greek"            "Greek"]
   ["halal"            "Halal"]
   ["hawaiian"         "Hawaiian"]
   ["himalayan"        "Himalayan/Nepalese"]
   ["hotdog"           "Hot Dogs"]
   ["hungarian"        "Hungarian"]
   ["indpak"           "Indian"]
   ["indonesian"       "Indonesian"]
   ["irish"            "Irish"]
   ["italian"          "Italian"]
   ["japanese"         "Japanese"]
   ["korean"           "Korean"]
   ["kosher"           "Kosher"]
   ["latin"            "Latin American"]
   ["raw_food"         "Live/Raw Food"]
   ["malaysian"        "Malaysian"]
   ["mediterranean"    "Mediterranean"]
   ["mexican"          "Mexican"]
   ["mideastern"       "Middle Eastern"]
   ["modern_european"  "Modern European"]
   ["mongolian"        "Mongolian"]
   ["moroccan"         "Moroccan"]
   ["pakistani"        "Pakistani"]
   ["persian"          "Persian/Iranian"]
   ["peruvian"         "Peruvian"]
   ["pizza"            "Pizza"]
   ["polish"           "Polish"]
   ["portuguese"       "Portuguese"]
   ["russian"          "Russian"]
   ["salad"            "Salad"]
   ["sandwiches"       "Sandwiches"]
   ["scandinavian"     "Scandinavian"]
   ["seafood"          "Seafood"]
   ["singaporean"      "Singaporean"]
   ["soulfood"         "Soul Food"]
   ["soup"             "Soup"]
   ["southern"         "Southern"]
   ["spanish"          "Spanish"]
   ["steak"            "Steakhouses"]
   ["sushi"            "Sushi Bars"]
   ["szechuan"         "Szechuan"]
   ["taiwanese"        "Taiwanese"]
   ["tapas"            "Tapas Bars"]
   ["tapasmallplates"  "Tapas/Small Plates"]
   ["tex-mex"          "Tex-Mex"]
   ["thai"             "Thai"]
   ["turkish"          "Turkish"]
   ["ukrainian"        "Ukrainian"]
   ["vegan"            "Vegan"]
   ["vegetarian"       "Vegetarian"]
   ["vietnamese"       "Vietnamese"]])

(defn surprise-me
  "Returns a random cuisine."
  []
  (first (rand-nth cuisines)))

(defn search-parameters
  "Formats the supplied parameters to be compliant with the Yelp API."
  [{:keys [cuisine latitude longitude distance]
    :or {cuisine (surprise-me)
         latitude "40.7127"
         longitude "-74.0059"
         distance "5"}}]
  {"term" "restaurants"
   "limit" "20"
   "sort" "2"
   "category_filter" (if (= cuisine "surprise_me") (surprise-me) cuisine)
   "ll" (str latitude "," longitude)
   "radius_filter" (miles->meters (min 25 (bigdec distance)))})

(defn search
  "Searches the Yelp API."
  [api params location]
  (client/business-search api (search-parameters (merge location params))))
