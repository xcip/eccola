(ns eccola.libs.calculations)

(defn miles->meters
  "Converts miles to meters."
  [miles]
  (* miles 1609))
