(ns eccola.views.home
  (:require [net.cgrand.enlive-html :as html]
            [eccola.config :refer [yelp-api]]
            [eccola.libs.yelp :as yelp]
            [clojure.string :as string]
            [cemerick.url :refer [url-encode]]))

(def default-thumbnail
  "http://s3-media2.ak.yelpcdn.com/bphoto/7DIHu8a0AHhw-BffrDIxPA/ms.jpg")

(def template-file "templates/home/index.html")

(html/defsnippet data-credits template-file [:#data-credits] [] identity)

(html/defsnippet cuisine-option template-file [:#cuisine :> html/last-child]
  [[option-value option-label]]

  [:option] (html/do->
              (html/set-attr :value option-value)
              (html/content option-label)))

(html/defsnippet restaurant-result template-file [:.restaurant-result]
  [{:keys [:name
           :rating_img_url
           :rating
           :review_count
           :mobile_url
           :image_url
           :directions_uri]}]

  [:.restaurant-name] (html/content name)
  [:.restaurant-image] (html/do->
                         (html/set-attr :src image_url)
                         (html/set-attr :alt name))

  [:.restaurant-rating-image] (html/do->
                                (html/set-attr :src rating_img_url)
                                (html/set-attr
                                  :alt (str rating " star rating")))

  [:.restaurant-number-reviews] (html/content
                                  (str "(" review_count " reviews)"))

  [:.restaurant-directions] (html/set-attr :href directions_uri)
  [:.restaurant-yelp-page] (html/set-attr :href mobile_url))

(html/deftemplate index-tpl template-file
  [restaurants search-performed? {:keys [cuisine distance]
                                  :or {cuisine "surprise_me" distance 5}}]

  [:#cuisine] (html/append (map cuisine-option yelp/cuisines))
  [:#cuisine :> (html/attr= :value cuisine)] (html/set-attr :selected "selected")

  [:#distance] (html/set-attr :value distance)
  [:#restaurant-results-list] (when-not (empty? restaurants)
                                (html/do->
                                  (html/content (map restaurant-result restaurants))
                                  (html/append (data-credits))))

  [:#no-results] (when (and (empty? restaurants) search-performed?) identity))

(defn daddr
  "Creates a destination address for Google Maps."
  [{:keys [address city state_code postal_code]}]
  (str (string/join " " address) ", " city ", " state_code " " postal_code))

(defn directions-uri
  "Creates a url that retrieves the directions from your current location to
  the business."
  [business]
  (str "http://maps.google.com/maps?saddr=Current%20Location&daddr="
       (url-encode (daddr (:location business)))))

(defn format-restaurants
  "Returns a map containing the desired restaurant data."
  [business]
  (assoc (select-keys business [:name
                                :rating_img_url
                                :rating
                                :review_count
                                :mobile_url])
         :image_url (get business :image_url default-thumbnail)
         :directions_uri (directions-uri business)))

(defn search-results->restaurants [search-results]
  "Formats the search-results into a vector of restaurants."
  (vec (map format-restaurants (:businesses search-results))))

(defn index
  "Returns the HTML output for the home page."
  [params location]
  (if (empty? params)
    (index-tpl {} false params)
    (let [restaurants
          (search-results->restaurants (yelp/search yelp-api params location))]
      (index-tpl (shuffle restaurants) true params))))
