(ns eccola.core
  (:gen-class)
  (:require [ring.middleware.reload :as reload :refer [wrap-reload]]
            [org.httpkit.server :refer [run-server]]
            [compojure.handler :refer [site]]
            [compojure.core :refer [defroutes GET]]
            [compojure.route :as route]
            [environ.core :refer [env]]
            [eccola.views.home :as home-view]
            [clj-maxmind-geoip.core :as geoip]))

(geoip/init-geoip "resources/GeoLiteCity.dat")

(defn location
  "Retrieves the geo-ip location information based on the supplied IP."
  [ip]
  (geoip/lookup-location ip))

(defn ip-address
  "Returns the IP address of the requesting machine. Uses x-forwarded-for
  in the event this is served by reverse proxy."
  [request]
  (or ((:headers request) "x-forwarded-for")
      (:remote-addr request)))

(defn remove-empty-keys
  "Removes any keys from coll that have values of empty strings."
  [coll]
  (into {} (remove #(= "" (val %)) coll)))

(defroutes routes
  (GET "/" request
       (let [ip-location (location (ip-address request))]
         (home-view/index (remove-empty-keys (:params request)) ip-location)))
  (route/resources "/")
  (route/not-found "<h1>Not Found</h1>"))

(def app (site routes))

(defn -main [& args]
  (let [handler (if (env :hot-reload?) (reload/wrap-reload #'app) app)]
    (run-server handler {:port 9090})))
