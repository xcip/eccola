(ns eccola.config
  (:require [environ.core :refer [env]]
            [gws.yelp.client :as yelp-client]))

(def yelp-api
  (yelp-client/create
    (env :yelp-consumer-key)
    (env :yelp-consumer-secret)
    (env :yelp-token)
    (env :yelp-token-secret)))
