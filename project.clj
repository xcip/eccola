(defproject eccola "0.1.0-SNAPSHOT"
  :description "A restaurant suggestion service for those times when you're too hungry to sift through hundreds of reviews."
  :url "https://gitlab.com/xcip/eccola"
  :license {:name "MIT License"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [http-kit "2.1.18"]
                 [environ "0.5.0"]
                 [enlive "1.1.5"]
                 [gws/yelp "0.2.0"]
                 [ring "1.3.0-RC1"]
                 [compojure "1.1.8"]
                 [com.cemerick/url "0.1.1"]
                 [clj-maxmind-geoip "0.1.0-SNAPSHOT"]]
  :main eccola.core
  :profiles {:uberjar {:aot :all}})
